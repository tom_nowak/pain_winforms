﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace Pain
{
    class LiteraryGenreControl : System.Windows.Forms.UserControl
    {
        public event EventHandler GenreChanged;
        Bitmap imagePoetry;
        Bitmap imageCriminal;
        Bitmap imageFantasy;
        Book.LiteraryGenre genre;

        public Book.LiteraryGenre Genre
        {
            get
            {
                return genre;
            }

            set
            {
                genre = value;
                this.Refresh();
                OnGenreChanged();
            }
        }

        protected virtual void OnGenreChanged()
        {
            EventHandler handler = GenreChanged;
            if (handler != null)
            {
                handler(this, null);
            }
        }
        
        public LiteraryGenreControl(Book.LiteraryGenre initialGenre)
        {
            this.genre = initialGenre;
            this.imagePoetry = new Bitmap(Properties.Resources.poetry); 
            this.imageCriminal = new Bitmap(Properties.Resources.criminal); 
            this.imageFantasy = new Bitmap(Properties.Resources.fantasy); 
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true );
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            this.Refresh();
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            TextureBrush tBrush = new TextureBrush(GetCurrentImage());
            e.Graphics.FillRectangle(tBrush, new Rectangle(0, 0, this.Width, this.Height));
        }

        protected override void OnMouseDown(System.Windows.Forms.MouseEventArgs e)
        {
            NextGenre();
        }

        private Image GetCurrentImage()
        {
            if (genre == Book.LiteraryGenre.poetry)
                return new Bitmap(imagePoetry, this.Size);
            else if (Genre == Book.LiteraryGenre.criminal)
                return new Bitmap(imageCriminal, this.Size);
            else
                return new Bitmap(imageFantasy, this.Size);
        }

        private void NextGenre()
        {
            if (Genre == Book.LiteraryGenre.poetry)
                Genre = Book.LiteraryGenre.criminal;
            else if (Genre == Book.LiteraryGenre.criminal)
                Genre = Book.LiteraryGenre.fantasy;
            else
                Genre = Book.LiteraryGenre.poetry;
        }
    }

}
