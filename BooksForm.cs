﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pain
{
    public partial class BooksForm: System.Windows.Forms.Form, IBooksDocumentObserver
    {
        // Additional data:
   
        private enum FilterState { showAll, showOld, showNew }

        IBooksDocument document;
        FilterState state = FilterState.showAll;

        // Setup:

        public BooksForm(IBooksDocument document)
        {
            InitializeComponent();
            this.document = document;
            CreateViewFromScratch();
            SetUpEventHandlers();
        }

        private void SetUpEventHandlers()
        {
            contextMenuAdd.Click += (sender, eventArgs) => RunAddForm();
            contextMenuEdit.Click += (sender, eventArgs) => RunEditForm();
            contextMenuDelete.Click += (sender, eventArgs) => DeleteSelected();
            menuAdd.Click += (sender, eventArgs) => RunAddForm();
            menuEdit.Click += (sender, eventArgs) => RunEditForm();
            menuDelete.Click += (sender, eventArgs) => DeleteSelected();
            menuShowAll.Click += (sender, eventArgs) => SetFilter(FilterState.showAll);
            menuShowNew.Click += (sender, eventArgs) => SetFilter(FilterState.showNew);
            menuShowOld.Click += (sender, eventArgs) => SetFilter(FilterState.showOld);
        }

        // IBooksDocumentObserver implementation:

        void IBooksDocumentObserver.OnBookAdded(Book book)
        {
            AddItemToViewIfVisible(book);
        }

        void IBooksDocumentObserver.OnBooksRemoved(HashSet<Book> removedBooks)
        {
            for (int i = booksListView.Items.Count - 1; i >= 0; i--)
            {
                if (removedBooks.Contains(booksListView.Items[i].Tag))
                    booksListView.Items[i].Remove();
            }
            UpdateStatusLabel();
        }
  
        void IBooksDocumentObserver.OnBookEdited(Book book)
        {
            foreach (ListViewItem item in booksListView.Items)
            {
                if (item.Tag == book)
                {
                    UpdateOrRemove(item);
                    return;
                }
            }
            AddItemToViewIfVisible(book);
        }

        // Main actions (called as event handlers):

        private void RunAddForm()
        {
            BookForm bookForm = new BookForm(null, document.GetAll());
            if(bookForm.ShowDialog() == DialogResult.OK)
            {
                Book newBook = new Book(bookForm.BookTitle, bookForm.BookAuthor, bookForm.BookPublicationDate, bookForm.BookGenre);
                AddItemToViewIfVisible(newBook);
                document.Add(newBook, this);
            }
        }

        private void RunEditForm()
        {
            if (booksListView.SelectedItems.Count != 1)
                return;
            Book book = (Book)booksListView.SelectedItems[0].Tag;
            BookForm bookForm = new BookForm(book, document.GetAll());
            if (bookForm.ShowDialog() == DialogResult.OK)
            {
                book.Title = bookForm.BookTitle;
                book.Author = bookForm.BookAuthor;
                book.PublicationDate = bookForm.BookPublicationDate;
                book.Genre = bookForm.BookGenre;
                UpdateOrRemove(booksListView.SelectedItems[0]);
                document.Edit(book, this);
            }
        }

        private void DeleteSelected()
        {
            HashSet<Book> booksToRemove = new HashSet<Book>();
            foreach (ListViewItem item in booksListView.SelectedItems)
                booksToRemove.Add((Book)item.Tag);
            for (int i = booksListView.Items.Count - 1; i >= 0; i--)
            {
                if (booksListView.Items[i].Selected)
                    booksListView.Items[i].Remove();
            }
            UpdateStatusLabel();
            document.Remove(booksToRemove, this);
        }

        private void SetFilter(FilterState filterState)
        {
            if (state == filterState)
                return;
            FilterState oldState = state;
            state = filterState;
            if (oldState == FilterState.showAll)
                FilterOutBooks();
            else
                CreateViewFromScratch();
            UpdateMenuFilterIcon();
        }

        // Helper functions:

        private void AddItemToViewIfVisible(Book newBook)
        {
            if (!IsBookVisible(newBook))
                return;
            ListViewItem item = new ListViewItem();
            item.Tag = newBook;
            UpdateViewItem(item);
            booksListView.Items.Add(item);
            UpdateStatusLabel();
        }

        private void UpdateOrRemove(ListViewItem item)
        {
            Book book = (Book)item.Tag;
            if (!IsBookVisible(book))
            {
                booksListView.Items.Remove(item);
                UpdateStatusLabel();
            }
            else
                UpdateViewItem(item);
        }

        private void UpdateViewItem(ListViewItem item)
        {
            Book book = (Book)item.Tag;
            while (item.SubItems.Count < 4)
                item.SubItems.Add(new ListViewItem.ListViewSubItem());
            item.SubItems[0].Text = book.Title;
            item.SubItems[1].Text = book.Author;
            item.SubItems[2].Text = book.PublicationDate.ToShortDateString();
            item.SubItems[3].Text = book.Genre.ToString();
        }

        private void FilterOutBooks()
        {
            for (int i = booksListView.Items.Count - 1; i >= 0; i--)
            {
                Book book = (Book)booksListView.Items[i].Tag;
                if(!IsBookVisible(book))
                    booksListView.Items[i].Remove();
            }
            UpdateStatusLabel();
        }

        private void CreateViewFromScratch()
        {
            booksListView.Items.Clear();
            foreach (Book book in document.GetAll())
            {
                if (IsBookVisible(book))
                    AddItemToViewIfVisible(book);
            }
            UpdateStatusLabel();
        }

        private void UpdateMenuFilterIcon()
        {
            if (state == FilterState.showOld)
                menuFilter.Image = Properties.Resources.show_old;
            else if (state == FilterState.showNew)
                menuFilter.Image = Properties.Resources.show_new;
            else
                menuFilter.Image = Properties.Resources.show_all;
        }

        private void UpdateStatusLabel()
        {
            statusLabel.Text = "total: " + booksListView.Items.Count;
        }

        private bool IsBookVisible(Book book)
        {
            if(state == FilterState.showOld)
                return book.PublicationDate.Year < 2000;
            if(state == FilterState.showNew)
                return book.PublicationDate.Year >= 2000;
            return true;
        }
    }
}
