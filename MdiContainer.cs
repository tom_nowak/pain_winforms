﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pain
{
    public interface IBooksDocument
    {
        List<Book> GetAll();
        void Add(Book book, object caller);
        void Remove(HashSet<Book> books, object caller);
        void Edit(Book book, object caller);
    }

    public interface IBooksDocumentObserver
    {
        void OnBookAdded(Book book);
        void OnBooksRemoved(HashSet<Book> removedBooks);
        void OnBookEdited(Book book);
    }

    public partial class MdiContainer : System.Windows.Forms.Form, IBooksDocument
    {
        private List<Book> books = new List<Book>();
        private HashSet<IBooksDocumentObserver> views;

        public MdiContainer()
        {
            InitializeComponent();
            SetUpDefaultData();
            views = new HashSet<IBooksDocumentObserver>();
            AddNewView();
        }

        List<Book> IBooksDocument.GetAll()
        {
            return books;
        }

        void IBooksDocument.Add(Book book, object caller)
        {
            books.Add(book);
            foreach(IBooksDocumentObserver view in views)
            {
                if (!Object.ReferenceEquals(caller, view))
                    view.OnBookAdded(book);
            }
        }

        void IBooksDocument.Remove(HashSet<Book> removedBooks, object caller)
        {
            books = books.Except(removedBooks).ToList();
            foreach(IBooksDocumentObserver view in views)
            {
                if (!Object.ReferenceEquals(caller, view))
                    view.OnBooksRemoved(removedBooks);
            }
        }

        void IBooksDocument.Edit(Book book, object caller)
        {
            foreach(IBooksDocumentObserver view in views)
            {
                if (!Object.ReferenceEquals(caller, view))
                    view.OnBookEdited(book);
            }
        }

        private void AddNewView()
        {
            BooksForm bookForm = new BooksForm((IBooksDocument)this);
            bookForm.MdiParent = this;
            bookForm.FormClosing += F_FormClosing;
            bookForm.FormClosed += F_FormClosed;
            views.Add((IBooksDocumentObserver)bookForm);
            bookForm.Show();
        }

        private void NewViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewView();
        }

        private void F_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (views.Count == 1 && e.CloseReason == CloseReason.UserClosing)
                e.Cancel = true;
            else
                e.Cancel = false;
        }

        private void F_FormClosed(object sender, FormClosedEventArgs e)
        {
            views.Remove((IBooksDocumentObserver)sender);
        }

        private void SetUpDefaultData()
        {
            books.Add(new Pain.Book("The Lord Of The Rings", "J.R.R. Tolkien", new DateTime(1954, 7, 29), Book.LiteraryGenre.fantasy));
            books.Add(new Pain.Book("Ten Little Niggers", "Agatha Christie", new DateTime(1939, 6, 11), Book.LiteraryGenre.criminal));
            books.Add(new Pain.Book("Pan Cogito", "Zbigniew Herbert", new DateTime(1974, 1, 1), Book.LiteraryGenre.poetry));
            books.Add(new Pain.Book("Jako ptaki Bieszczadu", "Ryszard Szociński", new DateTime(2002, 1, 1), Book.LiteraryGenre.poetry));
            books.Add(new Pain.Book("Harry Potter and the Half-Blood Prince", "J. K. Rowling", new DateTime(2005, 7, 16), Book.LiteraryGenre.fantasy));
            books.Add(new Pain.Book("Solaris", "Stanisław Lem", new DateTime(1961, 1, 2), Book.LiteraryGenre.fantasy));
        }
    }
}
