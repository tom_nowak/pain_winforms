﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pain
{
    public partial class BookForm : Form
    {
        private Book book;
        private List<Book> books;
        private LiteraryGenreControl genreControl;

        public BookForm(Book book, List<Book> books)
        {
            InitializeComponent();
            this.book = book;
            this.books = books;
            this.genreControl = new LiteraryGenreControl(Book.LiteraryGenre.poetry);
            this.tableLayoutPanel1.Controls.Add(genreControl, 1, 3);
            this.genreControl.GenreChanged += (sender, eventArgs) => UpdateGenreLabel();
            UpdateGenreLabel();
        }

        public string BookTitle
        {
            get { return titlePicker.Text; }
        }

        public string BookAuthor
        {
            get { return authorPicker.Text; }
        }

        public DateTime BookPublicationDate
        {
            get { return publicationDatePicker.Value; }
        }

        public Book.LiteraryGenre BookGenre
        {
            get { return genreControl.Genre; }
        }

        private void UpdateGenreLabel()
        {
            genreLabel.Text = "Genre (" + genreControl.Genre.ToString() + ")";
        }

        private void ok_Click(object sender, EventArgs e)
        {
            if (ValidateChildren() && ValidateTitleAuthorUniqueness())
                DialogResult = DialogResult.OK;
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            UnsetTitleAuthorErrorProvider();
            DialogResult = DialogResult.Cancel;
        }

        private void BookForm_Load(object sender, EventArgs e)
        {
            if (book == null)
                return;
            titlePicker.Text = book.Title;
            authorPicker.Text = book.Author;
            publicationDatePicker.Value = book.PublicationDate;
            genreControl.Genre = book.Genre;
        }

        private void BookForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = false;
        }

        private void titlePicker_Validating(object sender, CancelEventArgs e)
        {
            ValidateTextBox(titlePicker, e);
        }

        private void titlePicker_Validated(object sender, EventArgs e)
        {
            UnsetTitleAuthorErrorProvider();
        }

        private void authorPicker_Validating(object sender, CancelEventArgs e)
        {
            ValidateTextBox(authorPicker, e);
        }

        private void authorPicker_Validated(object sender, EventArgs e)
        {
            UnsetTitleAuthorErrorProvider();
        }
  
        private void publicationDatePicker_Validating(object sender, CancelEventArgs e)
        {
            if(publicationDatePicker.Value > DateTime.Now)
            {
                e.Cancel = true;
                errorProvider.SetError(publicationDatePicker, "publication date cannot be future date");
            }
        }

        private void publicationDatePicker_Validated(object sender, EventArgs e)
        {
            errorProvider.SetError(publicationDatePicker, "");
        }

        private void ValidateTextBox(TextBox textBox, CancelEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textBox.Text))
                    throw new Exception("cannot be empty");
                if (!char.IsLetterOrDigit(textBox.Text[0]))
                    throw new Exception("must start with letter or digit");
            }
            catch (Exception exception)
            {
                e.Cancel = true;
                errorProvider.SetError(textBox, exception.Message);
            }
        }

        private bool ValidateTitleAuthorUniqueness()
        {
            foreach (Book book in books)
            {
                if (book.Title == titlePicker.Text && book.Author == authorPicker.Text)
                {
                    if (this.book != book)  // if book with repeating (title, author) is not being edited
                    {
                        errorProvider.SetError(titlePicker, "(title, author) pair must be uniqe");
                        errorProvider.SetError(authorPicker, "(title, author) pair must be uniqe");
                        return false;
                    }
                }
            }
            return true;
        }

        private void UnsetTitleAuthorErrorProvider()
        {
            errorProvider.SetError(titlePicker, "");
            errorProvider.SetError(authorPicker, "");
        }
    }
}
