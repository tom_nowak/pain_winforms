﻿using System;

namespace Pain
{
    public class Book
    {
        public enum LiteraryGenre { poetry, fantasy, criminal }

        public string Title
        {
            get;
            set;
        }

        public string Author
        {
            get;
            set;
        }

        public DateTime PublicationDate
        {
            get;
            set;
        }

        public LiteraryGenre Genre
        {
            get;
            set;
        }

        public Book(string title, string author, DateTime publicationDate, LiteraryGenre genre)
        {
            Title = title;
            Author = author;
            PublicationDate = publicationDate;
            Genre = genre;
        }
    }
}
